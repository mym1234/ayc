package com.lili.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lili.entity.Appointment;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
public interface AppointmentMapper extends BaseMapper<Appointment> {

}
