package com.lili.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lili.entity.Case;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
public interface CaseMapper extends BaseMapper<Case> {

}
