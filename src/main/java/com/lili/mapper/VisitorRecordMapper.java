package com.lili.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lili.entity.VisitorRecord;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
public interface VisitorRecordMapper extends BaseMapper<VisitorRecord> {

}
