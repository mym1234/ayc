package com.lili.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lili.entity.Doctor;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mym
 * @since 2023-10-22
 */
public interface DoctorMapper extends BaseMapper<Doctor> {

}
