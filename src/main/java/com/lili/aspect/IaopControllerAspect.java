package com.lili.aspect;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @author: mym
 * @description: Controller切面
 * @date: 2023/8/6 14:24
 */
@Configuration
@Aspect
@Slf4j
public class IaopControllerAspect {

    @Around("@within(org.springframework.stereotype.Controller)")
    public Object doAroundAdviceRequestMapping(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        return this.doAroundAdvice(proceedingJoinPoint);
    }

    @Around("@within(org.springframework.web.bind.annotation.RestController)")
    public Object doAroundAdviceGetMapping(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        return this.doAroundAdvice(proceedingJoinPoint);
    }

    public Object doAroundAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object result = null;
        long start = System.currentTimeMillis();
        Exception exception = null;
        try {
            result = proceedingJoinPoint.proceed();
        } catch (Exception e) {
            exception = e;
            log.error("请求出错", e);
            throw e;
        } finally {
            long exeTime = System.currentTimeMillis() - start;
            String className = proceedingJoinPoint.getTarget().getClass().getName();
            String methodName = proceedingJoinPoint.getSignature().getName();
            Object[] args = proceedingJoinPoint.getArgs();
            StringBuffer argStr = new StringBuffer();
            if (args != null) {
                Object[] objects = args;
                int len = args.length;

                for (int i = 0; i < len; ++i) {
                    Object object = objects[i];
                    if (object != null && !(object instanceof ServletRequest) && !(object instanceof ServletResponse) && object.getClass().getName().indexOf("org.") < 0) {
                        try {
                            if (argStr.length() > 0) {
                                argStr.append(",");
                            }

                            String argJson = JSONObject.toJSONString(object);
                            if (argJson.length() > 102400) {
                                argJson = argJson.substring(0, 102400) + " ... ";
                            }

                            argStr.append(object.getClass().getSimpleName()).append(":").append(argJson);
                        } catch (Throwable var39) {
                            log.warn("Json Parse Error", var39);
                        }
                    }
                }
            }


            String resultJson = "";

//            try {
//                resultJson = JSONObject.toJSONString(result);
//            } catch (Exception e) {
//                resultJson = String.valueOf(result);
//                if (log.isDebugEnabled()) {
//                    log.debug(e.toString(), e);
//                }
//            }
//
//            if (resultJson.length() > 102400) {
//                resultJson = resultJson.substring(0, 102400) + " ... ";
//            }


            if (className != null && !className.startsWith("org.springframework")) {
                log.info(":: Iaop 参数打印  {} - {} 入参: {}  耗时 {} 毫秒, {}", new Object[]{className, methodName, argStr, exeTime, exception != null ? "" : "返回数据: " + resultJson});
            }

        }
        return result;
    }

}
