package com.lili.convert;

import com.lili.entity.Appointment;
import com.lili.vo.AppointmentVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author: mym
 * @description: AppointmentConvert
 * @date: 2023/10/27 14:54
 */
@Mapper
public interface AppointmentConvert {

    /*
   MerchantDTO和Merchant实体类的转换
    */
    AppointmentConvert INSTANCE= Mappers.getMapper(AppointmentConvert.class);

    //  me---->DTO
    AppointmentVO entity2VO(Appointment entity);

    //DTO--->merchat;

    Appointment VO2Entity(AppointmentVO vo);
}
