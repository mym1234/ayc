package com.lili.convert;

import com.lili.entity.Article;
import com.lili.entity.City;
import com.lili.vo.ArticleVO;
import com.lili.vo.CityVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ArticleConvert {

    /*
    MerchantDTO和Merchant实体类的转换
     */
    ArticleConvert INSTANCE=Mappers.getMapper(ArticleConvert.class);

//  me---->DTO
@Mapping(source = "sortTime", target = "sortTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    ArticleVO article2VO(Article article);

    //DTO--->merchat;

    @Mapping(source = "sortTime", target = "sortTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    Article VO2Article(ArticleVO articleVO);

//    @Mapping(source = "sortTime", target = "sortTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
//    List<ArticleVO> listentity2dto(List<Article> list);


}
