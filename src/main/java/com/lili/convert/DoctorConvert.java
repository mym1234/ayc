package com.lili.convert;

import com.lili.entity.Case;
import com.lili.entity.Doctor;
import com.lili.vo.CaseVO;
import com.lili.vo.DoctorVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DoctorConvert {

    /*
    MerchantDTO和Merchant实体类的转换
     */
    DoctorConvert INSTANCE=Mappers.getMapper(DoctorConvert.class);

//  me---->DTO
    @Mapping(source = "sortTime", target = "sortTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    DoctorVO doctor2VO(Doctor doc);

    //DTO--->merchat;

    @Mapping(source = "sortTime", target = "sortTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    Doctor VO2Doctor(DoctorVO doctorVO);

//    List<ArticleVO> listentity2dto(List<Article> list);


}
