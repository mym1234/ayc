package com.lili.convert;

import com.lili.entity.Article;
import com.lili.entity.Case;
import com.lili.vo.ArticleVO;
import com.lili.vo.CaseVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CaseConvert {

    /*
    MerchantDTO和Merchant实体类的转换
     */
    CaseConvert INSTANCE=Mappers.getMapper(CaseConvert.class);

//  me---->DTO
    @Mapping(source = "sortTime", target = "sortTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    CaseVO case2VO(Case ca);

    //DTO--->merchat;

    @Mapping(source = "sortTime", target = "sortTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    Case VO2Case(CaseVO caseVO);

//    List<ArticleVO> listentity2dto(List<Article> list);


}
