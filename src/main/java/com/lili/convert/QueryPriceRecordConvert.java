package com.lili.convert;

import com.lili.entity.City;
import com.lili.entity.QueryPriceRecord;
import com.lili.vo.CityVO;
import com.lili.vo.QueryPriceRecordVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author: mym
 * @description: QueryPriceRecordConvert
 * @date: 2023/10/27 14:54
 */
@Mapper
public interface QueryPriceRecordConvert {

    /*
   MerchantDTO和Merchant实体类的转换
    */
    QueryPriceRecordConvert INSTANCE= Mappers.getMapper(QueryPriceRecordConvert.class);

    //  me---->DTO
    QueryPriceRecordVO entity2VO(QueryPriceRecord entity);

    //DTO--->merchat;

    QueryPriceRecord VO2Entity(QueryPriceRecordVO vo);
}
