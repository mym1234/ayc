package com.lili.convert;

import com.lili.entity.City;
import com.lili.vo.CityVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CityConvert {

    /*
    MerchantDTO和Merchant实体类的转换
     */
    CityConvert INSTANCE=Mappers.getMapper(CityConvert.class);

//  me---->DTO
    @Mapping(source = "sortTime", target = "sortTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    CityVO city2VO(City city);

    //DTO--->merchat;

    @Mapping(source = "sortTime", target = "sortTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
    City VO2City(CityVO cityVO);

//    @Mapping(source = "sortTime", target = "sortTime", dateFormat = "yyyy-MM-dd HH:mm:ss")
//    List<CityVO> listentity2dto(List<City> list);


}
