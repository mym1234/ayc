package com.lili.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lili.entity.City;
import com.lili.qo.CityQo;
import com.lili.vo.CityVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
public interface ICityService extends IService<City> {

    CityVO queryById(Long id);

    boolean add(CityVO vo);

    CityVO update(CityVO vo);

    CityVO deleteById(Long id);

    IPage pageQuery(CityQo qo);

    Map<Long, List<City>> selectListByIds(List<Long> cityIdList);
}
