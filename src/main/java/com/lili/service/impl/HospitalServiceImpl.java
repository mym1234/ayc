package com.lili.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lili.entity.Hospital;
import com.lili.mapper.HospitalMapper;
import com.lili.service.IHospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mym
 * @since 2023-10-28
 */
@Service
public class HospitalServiceImpl extends ServiceImpl<HospitalMapper, Hospital> implements IHospitalService {

    @Autowired
    HospitalMapper hospitalMapper;

    @Override
    public List<Hospital> selectList(Long id) {
        QueryWrapper<Hospital> queryWrapper = new QueryWrapper();
        queryWrapper.eq("doctor_id",id);
        List<Hospital> hospitals = hospitalMapper.selectList(queryWrapper);
        return hospitals;
    }

    @Override
    public Map<Long, List<Hospital>> selectListByIds(List<Long> idList) {
        QueryWrapper<Hospital> queryWrapper = new QueryWrapper();
        queryWrapper.in("doctor_id",idList);
        List<Hospital> hospitals = hospitalMapper.selectList(queryWrapper);
        Map<Long, List<Hospital>> retMap = hospitals.stream().collect(groupingBy(Hospital::getDoctorId));
        return retMap;
    }

    @Override
    public void deleteByDoctorId(Long id) {
        QueryWrapper<Hospital> queryWrapper = new QueryWrapper();
        queryWrapper.eq("doctor_id",id);
        hospitalMapper.delete(queryWrapper);
    }
}
