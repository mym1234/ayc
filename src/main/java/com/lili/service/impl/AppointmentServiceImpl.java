package com.lili.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lili.convert.AppointmentConvert;
import com.lili.convert.QueryPriceRecordConvert;
import com.lili.entity.Appointment;
import com.lili.entity.QueryPriceRecord;
import com.lili.entity.VisitorRecord;
import com.lili.mapper.AppointmentMapper;
import com.lili.qo.AppointmentQo;
import com.lili.service.IAppointmentService;
import com.lili.util.IPUtils;
import com.lili.vo.AppointmentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
@Service
public class AppointmentServiceImpl extends ServiceImpl<AppointmentMapper, Appointment> implements IAppointmentService {

    @Autowired
    AppointmentMapper appointmentMapper;

    @Override
    public IPage pageQuery(AppointmentQo qo) {
        QueryWrapper<Appointment> queryWrapper = new QueryWrapper();
        if(!Objects.isNull(qo.getDoctorId())){
            queryWrapper.eq("doctor_id",qo.getDoctorId());
        }
        if(!StringUtils.isEmpty(qo.getKey())){
            queryWrapper.like("phone_number",qo.getKey()).or().like("patient_name",qo.getKey());
        }
        queryWrapper.orderByDesc("order_time");


        // 第一个参数：当前页   第二个参数：每页显示条数
        Page<Appointment> page = new Page(qo.getCurrent(), qo.getSize());
        IPage<Appointment> resultPage = appointmentMapper.selectPage(page, queryWrapper);
        return resultPage;
    }

    @Override
    public boolean add(AppointmentVO vo) {
        Appointment appointment = AppointmentConvert.INSTANCE.VO2Entity(vo);
        boolean flag = this.save(appointment);
        return flag;
    }
}
