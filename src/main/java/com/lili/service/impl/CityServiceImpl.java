package com.lili.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lili.convert.ArticleConvert;
import com.lili.convert.CityConvert;
import com.lili.entity.Article;
import com.lili.entity.City;
import com.lili.entity.Hospital;
import com.lili.mapper.CityMapper;
import com.lili.qo.CityQo;
import com.lili.service.ICityService;
import com.lili.vo.CityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.stream.Collectors.groupingBy;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City> implements ICityService {

    @Autowired
    private CityMapper cityMapper;

    @Override
    public CityVO queryById(Long id) {
        City city = this.getById(id);
        CityVO vo = CityConvert.INSTANCE.city2VO(city);
        return vo;
    }

    @Override
    public boolean add(CityVO vo) {
        LambdaQueryWrapper<City> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(City::getCityName,vo.getCityName());
        Integer count = cityMapper.selectCount(queryWrapper);
        if(count >= 1){
            return false;
        }
        City city = CityConvert.INSTANCE.VO2City(vo);
        boolean flag = this.save(city);
        return flag;
    }

    @Override
    public CityVO update(CityVO vo) {
        City city = this.getById(vo.getId());
        if(!Objects.isNull(city)){
            City c = CityConvert.INSTANCE.VO2City(vo);
            c.setId(city.getId());
            this.updateById(c);
        }
        return vo;
    }

    @Override
    public CityVO deleteById(Long id) {
        City city = this.getById(id);
        if(!Objects.isNull(city)){
            this.removeById(id);
        }
        CityVO vo = CityConvert.INSTANCE.city2VO(city);
        return vo;
    }

    @Override
    public IPage pageQuery(CityQo qo) {
        QueryWrapper<City> queryWrapper = new QueryWrapper();
        if(!StringUtils.isEmpty(qo.getKey())){
            queryWrapper.like("city_name",qo.getKey());
        }
        if(!StringUtils.isEmpty(qo.getIsFeatured())){
            queryWrapper.eq("is_featured",qo.getIsFeatured());
        }
        if(!StringUtils.isEmpty(qo.getIsPublished())){
            queryWrapper.eq("is_published",qo.getIsPublished());
        }
        queryWrapper.orderByDesc("sort_time");

        // 第一个参数：当前页   第二个参数：每页显示条数
        Page<City> page = new Page(qo.getCurrent(), qo.getSize());
        IPage<City> cityPage = cityMapper.selectPage(page, queryWrapper);
        return cityPage;
    }

    @Override
    public Map<Long, List<City>> selectListByIds(List<Long> cityIdList) {
        QueryWrapper<City> queryWrapper = new QueryWrapper();
        queryWrapper.in("id",cityIdList);
        List<City> citys = cityMapper.selectList(queryWrapper);
        Map<Long, List<City>> retMap = citys.stream().collect(groupingBy(City::getId));
        return retMap;
    }


}
