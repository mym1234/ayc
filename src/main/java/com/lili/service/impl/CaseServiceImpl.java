package com.lili.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lili.convert.ArticleConvert;
import com.lili.convert.CaseConvert;
import com.lili.entity.*;
import com.lili.mapper.ArticleMapper;
import com.lili.mapper.CaseMapper;
import com.lili.mapper.DoctorMapper;
import com.lili.qo.ArticleQo;
import com.lili.qo.CaseQo;
import com.lili.service.ICaseService;
import com.lili.service.ICityService;
import com.lili.vo.ArticleVO;
import com.lili.vo.CaseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
@Service
public class CaseServiceImpl extends ServiceImpl<CaseMapper, Case> implements ICaseService {

    @Autowired
    CaseMapper caseMapper;

    @Autowired
    DoctorMapper doctorMapper;

    @Autowired
    ICityService iCityService;

    @Override
    public IPage<CaseVO> pageQuery(CaseQo qo) {
        QueryWrapper<Case> queryWrapper = new QueryWrapper();
        if(!StringUtils.isEmpty(qo.getKey())){
            queryWrapper.like("case_name",qo.getKey());
        }
        if(!StringUtils.isEmpty(qo.getIsPublished())){
            queryWrapper.eq("is_published",qo.getIsPublished());
        }
        if(!Objects.isNull(qo.getDoctorId())){
            queryWrapper.eq("doctor_id",qo.getDoctorId());
        }
        if(!Objects.isNull(qo.getCityId())){
            queryWrapper.eq("city_id",qo.getCityId());
        }
        queryWrapper.orderByDesc("sort_time");

        // 第一个参数：当前页   第二个参数：每页显示条数
        Page<Case> page = new Page(qo.getCurrent(), qo.getSize());
        IPage<Case> cityPage = caseMapper.selectPage(page, queryWrapper);
        List<Long> idList = cityPage.getRecords().stream().map(Case::getDoctorId).collect(Collectors.toList());
        Map<Long, List<Doctor>> docMap = null;
        if(idList.size() > 0){
            List<Doctor> doctorList = doctorMapper.selectBatchIds(idList);
            docMap = doctorList.stream().collect(groupingBy(Doctor::getId));
        }
        Map<Long, List<Doctor>> retMap = docMap;
        //获取城市列表
        List<Long> cityIdList = cityPage.getRecords().stream().map(Case::getCityId).collect(Collectors.toList());
        Map<Long,List<City>> cityMap = null;
        if(idList != null && cityIdList.size() > 0){
            cityMap = iCityService.selectListByIds(cityIdList);
        }
        Map<Long,List<City>> cMap = cityMap;
        IPage<CaseVO> returnPage = cityPage.convert(item -> {
            CaseVO vo = CaseConvert.INSTANCE.case2VO(item);
            if(retMap.containsKey(item.getDoctorId())){
                List<Doctor> docList = retMap.get(item.getDoctorId());
                Doctor doc = docList.get(0);
                vo.setGender(doc.getGender());
                vo.setDoctorAvatarUrl(doc.getDoctorAvatarUrl());
            }
            if(cMap.containsKey(item.getCityId())){
                List<City> list = cMap.get(item.getCityId());
                if(list.size() > 0){
                    vo.setCityName(list.get(0).getCityName());
                }
            }
            return vo;

        });
        return returnPage;
    }

    @Override
    public CaseVO add(CaseVO vo) {
        Case ca = CaseConvert.INSTANCE.VO2Case(vo);
        this.save(ca);
        vo.setId(ca.getId());
        return vo;
    }

    @Override
    public Case queryById(Long id) {
        Case ca = this.getById(id);
        return ca;
    }

    @Override
    public CaseVO update(CaseVO vo) {
        Case ca = this.getById(vo.getId());
        if(!Objects.isNull(ca)){
            Case cas = CaseConvert.INSTANCE.VO2Case(vo);
            cas.setId(ca.getId());
            this.updateById(cas);
        }
        return vo;
    }

    @Override
    public Case deleteById(Long id) {
        Case ca = this.getById(id);
        if(!Objects.isNull(ca)){
            this.removeById(ca);
        }
        return ca;
    }

}
