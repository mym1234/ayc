package com.lili.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lili.entity.QueryPriceRecord;
import com.lili.entity.VisitorRecord;
import com.lili.mapper.QueryPriceRecordMapper;
import com.lili.mapper.VisitorRecordMapper;
import com.lili.qo.QueryPriceQo;
import com.lili.qo.VisitorRecordQo;
import com.lili.service.IVisitorRecordService;
import com.lili.util.IPUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
@Service
public class VisitorRecordServiceImpl extends ServiceImpl<VisitorRecordMapper, VisitorRecord> implements IVisitorRecordService {

    @Autowired
    VisitorRecordMapper visitorRecordMapper;

    @Override
    public IPage pageQuery(VisitorRecordQo qo) {
        QueryWrapper<VisitorRecord> queryWrapper = new QueryWrapper();
        if(!StringUtils.isEmpty(qo.getKey())){
            queryWrapper.like("phone_number",qo.getKey());
        }
        if(!Objects.isNull(qo.getStart()) && !Objects.isNull(qo.getEnd())){
            queryWrapper.ge("last_visit_time",qo.getStart())
                    .le("last_visit_time",qo.getEnd());
        }
        queryWrapper.orderByDesc("first_visit_time");

        // 第一个参数：当前页   第二个参数：每页显示条数
        Page< VisitorRecord> page = new Page(qo.getCurrent(), qo.getSize());
        IPage<VisitorRecord> resultPage = visitorRecordMapper.selectPage(page, queryWrapper);
        return resultPage;
    }

    @Override
    public VisitorRecord markById(Long id) {
        VisitorRecord record = this.getById(id);
        if(!Objects.isNull(record)){
            record.setStatus(1);
            this.updateById(record);
        }
        return record;
    }

    @Override
    public void add(VisitorRecord record, HttpServletRequest request) {
        if(Objects.isNull(record) || StringUtils.isEmpty(record.getPhoneNumber())){
            return;
        }
        QueryWrapper<VisitorRecord> queryWrapper = new QueryWrapper();
        queryWrapper.like("phone_number",record.getPhoneNumber());
        VisitorRecord dbRecord = visitorRecordMapper.selectOne(queryWrapper);
        String ip = IPUtils.getIpAddr(request);
        if(Objects.isNull(dbRecord)){
            dbRecord = new VisitorRecord();
            dbRecord.setFirstVisitTime(new Date());
            dbRecord.setLastVisitTime(new Date());
            dbRecord.setLastVisitIp(ip);
            dbRecord.setStatus(0);
            dbRecord.setPhoneNumber(record.getPhoneNumber());
            dbRecord.setVisitCount(0);
            visitorRecordMapper.insert(dbRecord);
        }else{
            dbRecord.setLastVisitTime(new Date());
            dbRecord.setLastVisitIp(ip);
            dbRecord.setVisitCount(dbRecord.getVisitCount() + 1);
            visitorRecordMapper.updateById(dbRecord);
        }
    }
}
