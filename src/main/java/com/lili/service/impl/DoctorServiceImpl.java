package com.lili.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lili.convert.CaseConvert;
import com.lili.convert.DoctorConvert;
import com.lili.entity.Case;
import com.lili.entity.City;
import com.lili.entity.Doctor;
import com.lili.entity.Hospital;
import com.lili.mapper.CaseMapper;
import com.lili.mapper.DoctorMapper;
import com.lili.qo.CaseQo;
import com.lili.qo.DoctorQo;
import com.lili.service.ICityService;
import com.lili.service.IDoctorService;
import com.lili.service.IHospitalService;
import com.lili.vo.CaseVO;
import com.lili.vo.DoctorVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mym
 * @since 2023-10-22
 */
@Service
public class DoctorServiceImpl extends ServiceImpl<DoctorMapper, Doctor> implements IDoctorService {

    @Autowired
    DoctorMapper doctorMapper;

   @Autowired
   IHospitalService iHospitalService;

   @Autowired
   ICityService iCityService;

    @Override
    public IPage<DoctorVO> pageQuery(DoctorQo qo) {
        QueryWrapper<Doctor> queryWrapper = new QueryWrapper();
        if(!StringUtils.isEmpty(qo.getKey())){
            queryWrapper.like("doctor_name",qo.getKey());
        }
        if(!StringUtils.isEmpty(qo.getIsPublished())){
            queryWrapper.eq("is_published",qo.getIsPublished());
        }
        if(!StringUtils.isEmpty(qo.getIsFeatured())){
            queryWrapper.eq("is_featured",qo.getIsFeatured());
        }
        if(!Objects.isNull(qo.getDoctorType())){
            queryWrapper.eq("doctor_type",qo.getDoctorType());
        }
        if(!Objects.isNull(qo.getCityId())){
            queryWrapper.eq("city_id",qo.getCityId());
        }
        if(!StringUtils.isEmpty(qo.getExtKey())){
            queryWrapper.like("doctor_name",qo.getExtKey())
            .or().like("doctor_tags",qo.getExtKey())
            .or().like("education",qo.getExtKey())
            .or().like("practice_experience",qo.getExtKey())
            .or().like("professional_title",qo.getExtKey())
            .or().like("research_interests",qo.getExtKey())
            .or().like("social_positions",qo.getExtKey())
            .or().like("orthodontic_technique",qo.getExtKey());
        }
        queryWrapper.orderByDesc("sort_time");

        // 第一个参数：当前页   第二个参数：每页显示条数
        Page<Doctor> page = new Page(qo.getCurrent(), qo.getSize());
        IPage<Doctor> cityPage = doctorMapper.selectPage(page, queryWrapper);
        //获取医生列表
        List<Long> idList = cityPage.getRecords().stream().map(Doctor::getId).collect(Collectors.toList());
        Map<Long,List<Hospital>> hosMap = null;
        if(idList != null && idList.size() > 0){
            hosMap = iHospitalService.selectListByIds(idList);
        }
        Map<Long,List<Hospital>> hospitalMap = hosMap;
        //获取城市列表
        List<Long> cityIdList = cityPage.getRecords().stream().map(Doctor::getCityId).collect(Collectors.toList());
        Map<Long,List<City>> cityMap = null;
        if(idList != null && cityIdList.size() > 0){
            cityMap = iCityService.selectListByIds(cityIdList);
        }
        Map<Long,List<City>> cMap = cityMap;
        IPage<DoctorVO> returnPage = cityPage.convert(item -> {
            DoctorVO vo = DoctorConvert.INSTANCE.doctor2VO(item);
            if(hospitalMap.containsKey(item.getId())){
                List<Hospital> list = hospitalMap.get(item.getId());
                vo.setHospitalList(list);
            }
            if(cMap.containsKey(item.getCityId())){
                List<City> list = cMap.get(item.getCityId());
                if(list.size() > 0){
                    vo.setCityName(list.get(0).getCityName());
                }
            }
            return vo;
        });
        return returnPage;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public DoctorVO add(DoctorVO vo) {
        Doctor ca = DoctorConvert.INSTANCE.VO2Doctor(vo);
        this.save(ca);
        List<Hospital> hospitals =  vo.getHospitalList();
        if(hospitals != null && hospitals.size() >0){
            hospitals.forEach(hospital -> {
                hospital.setDoctorId(ca.getId());
            });
            iHospitalService.saveBatch(hospitals);
        }
        return vo;
    }

    @Override
    public DoctorVO queryById(Long id) {
        Doctor doc = this.getById(id);
        if(Objects.isNull(doc)){
            return null;
        }
        DoctorVO vo = DoctorConvert.INSTANCE.doctor2VO(doc);
        List<Hospital> hospitals =  iHospitalService.selectList(id);
        vo.setHospitalList(hospitals);
        return vo;
    }

    @Override
    public DoctorVO update(DoctorVO vo) {
        Doctor doc = this.getById(vo.getId());
        if(!Objects.isNull(doc)){
            Doctor doctor = DoctorConvert.INSTANCE.VO2Doctor(vo);
            doctor.setId(doc.getId());
            this.updateById(doctor);
            List<Hospital> hospitals =  vo.getHospitalList();
            if(hospitals != null && hospitals.size() > 0){
                iHospitalService.deleteByDoctorId(vo.getId());
                hospitals.forEach(hospital -> {
                    hospital.setDoctorId(vo.getId());
                });
                iHospitalService.saveBatch(hospitals);
            }

        }
        return vo;
    }

    @Override
    public Doctor deleteById(Long id) {
        Doctor doc = this.getById(id);
        if(!Objects.isNull(doc)){
            this.removeById(doc);
        }
        return doc;
    }
}
