package com.lili.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lili.convert.CityConvert;
import com.lili.convert.QueryPriceRecordConvert;
import com.lili.entity.City;
import com.lili.entity.QueryPriceRecord;
import com.lili.mapper.QueryPriceRecordMapper;
import com.lili.qo.QueryPriceQo;
import com.lili.service.IQueryPriceRecordService;
import com.lili.util.IPUtils;
import com.lili.vo.CityVO;
import com.lili.vo.QueryPriceRecordVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
@Service
public class QueryPriceRecordServiceImpl extends ServiceImpl<QueryPriceRecordMapper, QueryPriceRecord> implements IQueryPriceRecordService {

    @Autowired
    QueryPriceRecordMapper queryPriceRecordMapper;

    @Override
    public IPage pageQuery(QueryPriceQo qo) {
        QueryWrapper<QueryPriceRecord> queryWrapper = new QueryWrapper();
        if(!StringUtils.isEmpty(qo.getKey())){
            queryWrapper.like("phone_number",qo.getKey());
        }
        if(!Objects.isNull(qo.getStart()) && !Objects.isNull(qo.getEnd())){
            queryWrapper.ge("query_time",qo.getStart())
                    .le("query_time",qo.getEnd());
        }
        queryWrapper.orderByDesc("query_time");

        // 第一个参数：当前页   第二个参数：每页显示条数
        Page< QueryPriceRecord> page = new Page(qo.getCurrent(), qo.getSize());
        IPage<QueryPriceRecord> resultPage = queryPriceRecordMapper.selectPage(page, queryWrapper);
        return resultPage;
    }

    @Override
    public QueryPriceRecord markById(Long id) {
        QueryPriceRecord record = this.getById(id);
        if(!Objects.isNull(record)){
            record.setStatus(1);
            this.updateById(record);
        }
        return record;
    }

    @Override
    public boolean add(QueryPriceRecordVO vo, HttpServletRequest request) {
        QueryPriceRecord record = QueryPriceRecordConvert.INSTANCE.VO2Entity(vo);
        String ip = IPUtils.getIpAddr(request);
        record.setLastAccessIp(ip);
        record.setQueryTime(new Date());
        boolean flag = this.save(record);
        return flag;
    }
}
