package com.lili.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lili.convert.ArticleConvert;
import com.lili.convert.CityConvert;
import com.lili.entity.Article;
import com.lili.entity.City;
import com.lili.mapper.ArticleMapper;
import com.lili.qo.ArticleQo;
import com.lili.service.IArticleService;
import com.lili.vo.ArticleVO;
import com.lili.vo.CityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements IArticleService {

    @Autowired
    ArticleMapper articleMapper;

    @Override
    public IPage<Article> pageQuery(ArticleQo qo) {
        QueryWrapper<Article> queryWrapper = new QueryWrapper();
        if(!StringUtils.isEmpty(qo.getKey())){
            queryWrapper.like("article_name",qo.getKey());
        }
        if(!StringUtils.isEmpty(qo.getIsFeatured())){
            queryWrapper.eq("is_featured",qo.getIsFeatured());
        }
        if(!StringUtils.isEmpty(qo.getIsPublished())){
            queryWrapper.eq("is_published",qo.getIsPublished());
        }
        queryWrapper.orderByDesc("sort_time");
        queryWrapper.select("id","article_name","is_featured","sort_time","is_published","created_at","added_by");

        // 第一个参数：当前页   第二个参数：每页显示条数
        Page<Article> page = new Page(qo.getCurrent(), qo.getSize());
        IPage<Article> cityPage = articleMapper.selectPage(page, queryWrapper);
        return cityPage;
    }

    @Override
    public ArticleVO add(ArticleVO vo) {
        Article article = ArticleConvert.INSTANCE.VO2Article(vo);
        this.save(article);
        vo.setId(article.getId());
        return vo;
    }

    @Override
    public Article queryById(Long id) {
        Article article = this.getById(id);
        if(!Objects.isNull(article)){
            // 1. 更新的字段
            Article newArticle = new Article();
            newArticle.setInitRead(article.getInitRead() + 1);

            // 2.更新的条件
            QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("id",article.getId());

            articleMapper.update(newArticle,queryWrapper);
        }
        return article;
    }

    @Override
    public ArticleVO update(ArticleVO vo) {
        Article article = this.getById(vo.getId());
        if(!Objects.isNull(article)){
            Article art = ArticleConvert.INSTANCE.VO2Article(vo);
            art.setId(article.getId());
            this.updateById(art);
        }
        return vo;
    }

    @Override
    public Article deleteById(Long id) {
        Article article = this.getById(id);
        if(!Objects.isNull(article)){
            this.removeById(article);
        }
        return article;
    }
}
