package com.lili.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lili.entity.Article;
import com.lili.qo.ArticleQo;
import com.lili.vo.ArticleVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
public interface IArticleService extends IService<Article> {

    IPage<Article> pageQuery(ArticleQo qo);

    ArticleVO add(ArticleVO article);

    Article queryById(Long id);

    ArticleVO update(ArticleVO vo);

    Article deleteById(Long id);
}
