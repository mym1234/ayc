package com.lili.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lili.entity.Appointment;
import com.lili.qo.AppointmentQo;
import com.lili.vo.AppointmentVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
public interface IAppointmentService extends IService<Appointment> {

    IPage pageQuery(AppointmentQo qo);

    boolean add(AppointmentVO vo);
}
