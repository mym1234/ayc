package com.lili.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lili.entity.Article;
import com.lili.entity.Case;
import com.lili.qo.ArticleQo;
import com.lili.qo.CaseQo;
import com.lili.vo.ArticleVO;
import com.lili.vo.CaseVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
public interface ICaseService extends IService<Case> {

    IPage<CaseVO> pageQuery(CaseQo qo);

    CaseVO add(CaseVO article);

    Case queryById(Long id);

    CaseVO update(CaseVO vo);

    Case deleteById(Long id);
}
