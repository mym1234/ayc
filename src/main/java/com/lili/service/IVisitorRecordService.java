package com.lili.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lili.entity.VisitorRecord;
import com.lili.qo.QueryPriceQo;
import com.lili.qo.VisitorRecordQo;
import com.lili.vo.CityVO;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
public interface IVisitorRecordService extends IService<VisitorRecord> {

    IPage pageQuery(VisitorRecordQo qo);

    VisitorRecord markById(Long id);

    void add(VisitorRecord record, HttpServletRequest request);
}
