package com.lili.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lili.entity.Hospital;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mym
 * @since 2023-10-28
 */
public interface IHospitalService extends IService<Hospital> {

    List<Hospital> selectList(Long id);

    Map<Long, List<Hospital>> selectListByIds(List<Long> idList);

    void deleteByDoctorId(Long id);
}
