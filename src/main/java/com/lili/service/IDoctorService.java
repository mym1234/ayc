package com.lili.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lili.entity.Case;
import com.lili.entity.Doctor;
import com.lili.qo.CaseQo;
import com.lili.qo.DoctorQo;
import com.lili.vo.CaseVO;
import com.lili.vo.DoctorVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mym
 * @since 2023-10-22
 */
public interface IDoctorService extends IService<Doctor> {

    IPage<DoctorVO> pageQuery(DoctorQo qo);

    DoctorVO add(DoctorVO vo);

    DoctorVO queryById(Long id);

    DoctorVO update(DoctorVO vo);

    Doctor deleteById(Long id);

}
