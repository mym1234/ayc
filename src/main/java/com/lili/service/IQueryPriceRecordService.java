package com.lili.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lili.entity.QueryPriceRecord;
import com.lili.qo.QueryPriceQo;
import com.lili.vo.QueryPriceRecordVO;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mym
 * @since 2023-10-21
 */
public interface IQueryPriceRecordService extends IService<QueryPriceRecord> {

    IPage pageQuery(QueryPriceQo qo);

    QueryPriceRecord markById(Long id);

    boolean add(QueryPriceRecordVO vo, HttpServletRequest request);
}
