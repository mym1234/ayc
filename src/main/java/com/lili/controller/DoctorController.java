package com.lili.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lili.common.CommonResult;
import com.lili.entity.Case;
import com.lili.entity.Doctor;
import com.lili.qo.CaseQo;
import com.lili.qo.DoctorQo;
import com.lili.service.ICaseService;
import com.lili.service.IDoctorService;
import com.lili.util.IPUtils;
import com.lili.util.IpUtils2;
import com.lili.vo.CaseVO;
import com.lili.vo.DoctorVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;

/**
 *  医生管理
 *
 * @author mym
 * @since 2023-10-22
 */
@Controller
@RequestMapping("/doctor")
@Slf4j
public class DoctorController {

    @Autowired
    IDoctorService iDoctorService;

    /**
     * 根据id删除医生信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<Doctor> deleteById(@PathVariable("id") Long id) {
        Doctor doc = iDoctorService.deleteById(id);
        return CommonResult.success(doc);
    }


    /**
     * 根据id查询医生信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<DoctorVO> getArticleById(@PathVariable("id") Long id) {
        return CommonResult.success(iDoctorService.queryById(id));
    }

    /**
     * 修改医生 包括上架/下架
     *
     * @param vo
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<DoctorVO> update(@RequestBody DoctorVO vo) {
        DoctorVO result = iDoctorService.update(vo);
        return CommonResult.success(result);
    }


    /**
     * 新增医生
     *
     * @param vo
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<DoctorVO> add(@RequestBody DoctorVO vo) {
        DoctorVO a = iDoctorService.add(vo);
        return CommonResult.success(a);
    }


    /**
     * 分页查询
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/pageQuery", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<IPage<DoctorVO>> pageQuery(@RequestBody DoctorQo qo, HttpServletRequest request) {
        IPage<DoctorVO> page = iDoctorService.pageQuery(qo);
        return CommonResult.success(page);
    }

}
