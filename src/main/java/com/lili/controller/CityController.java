package com.lili.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lili.common.CommonResult;
import com.lili.entity.City;
import com.lili.qo.CityQo;
import com.lili.service.ICityService;
import com.lili.vo.CityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

/**
 *  城市信息
 *
 * @author mym
 * @since 2023-10-21
 */
@Controller
@RequestMapping("/city")
public class CityController {

    @Autowired
    ICityService iCityService;


    /**
     * 根据id查询城市信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CityVO> getCityById(@PathVariable("id") Long id) {
        return CommonResult.success(iCityService.queryById(id));
    }

    /**
     * 根据id删除城市信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<CityVO> deleteById(@PathVariable("id") Long id) {
        CityVO vo = iCityService.deleteById(id);
        return CommonResult.success(vo);
    }

    /**
     * 新增城市
     *
     * @param vo
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Boolean> add(@RequestBody CityVO vo) {
        boolean flag = iCityService.add(vo);
        if(!flag){
            return CommonResult.failed("城市名称重复");
        }
        return CommonResult.success(flag);
    }

    /**
     * 修改城市
     *
     * @param vo
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<CityVO> update(@RequestBody CityVO vo) {
        CityVO city = iCityService.update(vo);
        return CommonResult.success(city);
    }

    /**
     * 分页查询
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/pageQuery", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<IPage<City>> pageQuery(@RequestBody CityQo qo) {
        IPage cityPage = iCityService.pageQuery(qo);
        return CommonResult.success(cityPage);
    }





}
