package com.lili.controller;


import com.lili.common.CommonResult;
import com.lili.qo.UserLoginQo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Objects;

/**
 * 用户管理
 *
 * Created by mym on 2021/1/26.
 */
@Controller
@RequestMapping("/user")
public class UserController {



    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult login(@RequestBody UserLoginQo userLoginQo) {
        if(!Objects.isNull(userLoginQo) && userLoginQo.getUserName().equals("admin") && userLoginQo.getPassword().equals("llkj9088")){
            return CommonResult.success(true);
        }
        return CommonResult.failed("用户名或者密码错误");
    }


}
