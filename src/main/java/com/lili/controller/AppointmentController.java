package com.lili.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lili.common.CommonResult;
import com.lili.entity.Appointment;
import com.lili.entity.VisitorRecord;
import com.lili.qo.AppointmentQo;
import com.lili.qo.VisitorRecordQo;
import com.lili.service.IAppointmentService;
import com.lili.service.IVisitorRecordService;
import com.lili.vo.AppointmentVO;
import com.lili.vo.QueryPriceRecordVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 *  预约管理
 *
 * @author mym
 * @since 2023-10-21
 */
@Controller
@RequestMapping("/appointment")
public class AppointmentController {

    @Autowired
    IAppointmentService iAppointmentService;


    /**
     * 分页查询
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/pageQuery", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<IPage<Appointment>> pageQuery(@RequestBody AppointmentQo qo) {
        IPage page = iAppointmentService.pageQuery(qo);
        return CommonResult.success(page);
    }

    /**
     * 新增预约(小程序用)
     *
     * @param vo
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Boolean> add(@RequestBody AppointmentVO vo) {
        boolean flag = iAppointmentService.add(vo);
        return CommonResult.success(flag);
    }

}
