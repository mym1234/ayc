package com.lili.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lili.common.CommonResult;
import com.lili.entity.Article;
import com.lili.entity.Case;
import com.lili.qo.ArticleQo;
import com.lili.qo.CaseQo;
import com.lili.service.IArticleService;
import com.lili.service.ICaseService;
import com.lili.vo.ArticleVO;
import com.lili.vo.CaseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 *  案例管理
 *
 * @author mym
 * @since 2023-10-21
 */
@Controller
@RequestMapping("/case")
public class CaseController {

    @Autowired
    ICaseService iCaseService;

    /**
     * 根据id删除案例信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<Case> deleteById(@PathVariable("id") Long id) {
        Case ca = iCaseService.deleteById(id);
        return CommonResult.success(ca);
    }


    /**
     * 根据id查询案例信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<Case> getCaseById(@PathVariable("id") Long id) {
        return CommonResult.success(iCaseService.queryById(id));
    }

    /**
     * 修改案例 包括上架/下架
     *
     * @param vo
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<CaseVO> update(@RequestBody CaseVO vo) {
        CaseVO result = iCaseService.update(vo);
        return CommonResult.success(result);
    }


    /**
     * 新增案例
     *
     * @param vo
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<CaseVO> add(@RequestBody CaseVO vo) {
        CaseVO a = iCaseService.add(vo);
        return CommonResult.success(a);
    }


    /**
     * 分页查询
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/pageQuery", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<IPage<CaseVO>> pageQuery(@RequestBody CaseQo qo) {
        IPage<CaseVO> page = iCaseService.pageQuery(qo);
        return CommonResult.success(page);
    }

}
