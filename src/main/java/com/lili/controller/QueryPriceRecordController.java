package com.lili.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lili.common.CommonResult;
import com.lili.entity.QueryPriceRecord;
import com.lili.qo.QueryPriceQo;
import com.lili.service.IQueryPriceRecordService;
import com.lili.vo.QueryPriceRecordVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;

/**
 *  查询价格
 *
 * @author mym
 * @since 2023-10-21
 */
@Controller
@RequestMapping("/query-price")
public class QueryPriceRecordController {

    @Autowired
    IQueryPriceRecordService iQueryPriceRecordService;

    /**
     * 分页查询
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/pageQuery", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<IPage<QueryPriceRecord>> pageQuery(@RequestBody QueryPriceQo qo) {
        IPage page = iQueryPriceRecordService.pageQuery(qo);
        return CommonResult.success(page);
    }

    /**
     * 根据id标记
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "mark/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<QueryPriceRecord> markById(@PathVariable("id") Long id) {
        QueryPriceRecord record = iQueryPriceRecordService.markById(id);
        return CommonResult.success(record);
    }

    /**
     * 新增查询记录(小程序用)
     *
     * @param vo
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<Boolean> add(@RequestBody QueryPriceRecordVO vo, HttpServletRequest request) {
        boolean flag = iQueryPriceRecordService.add(vo,request);
        return CommonResult.success(flag);
    }

}
