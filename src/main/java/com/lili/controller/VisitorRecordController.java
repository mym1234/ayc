package com.lili.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lili.common.CommonResult;
import com.lili.entity.QueryPriceRecord;
import com.lili.entity.VisitorRecord;
import com.lili.qo.QueryPriceQo;
import com.lili.qo.VisitorRecordQo;
import com.lili.service.IQueryPriceRecordService;
import com.lili.service.IVisitorRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 *  访问记录
 *
 * @author mym
 * @since 2023-10-21
 */
@Controller
@RequestMapping("/visitor-record")
public class VisitorRecordController {

    @Autowired
    IVisitorRecordService iVisitorRecordService;

    /**
     * 分页查询
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/pageQuery", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<IPage<VisitorRecord>> pageQuery(@RequestBody VisitorRecordQo qo) {
        IPage page = iVisitorRecordService.pageQuery(qo);
        return CommonResult.success(page);
    }

    /**
     * 根据id标记
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "mark/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<VisitorRecord> markById(@PathVariable("id") Long id) {
        VisitorRecord record = iVisitorRecordService.markById(id);
        return CommonResult.success(record);
    }

}
