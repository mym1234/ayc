package com.lili.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lili.common.CommonResult;
import com.lili.entity.Article;
import com.lili.entity.City;
import com.lili.qo.ArticleQo;
import com.lili.qo.CityQo;
import com.lili.service.IArticleService;
import com.lili.vo.ArticleVO;
import com.lili.vo.CityVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 *  文章管理
 *
 * @author mym
 * @since 2023-10-21
 */
@Controller
@RequestMapping("/article")
public class ArticleController {

    @Autowired
    IArticleService iArticleService;

    /**
     * 根据id删除文章信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<Article> deleteById(@PathVariable("id") Long id) {
        Article article = iArticleService.deleteById(id);
        return CommonResult.success(article);
    }


    /**
     * 根据id查询文章信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<Article> getArticleById(@PathVariable("id") Long id) {
        return CommonResult.success(iArticleService.queryById(id));
    }

    /**
     * 修改文章 包括上架/下架
     *
     * @param vo
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<ArticleVO> update(@RequestBody ArticleVO vo) {
        ArticleVO result = iArticleService.update(vo);
        return CommonResult.success(result);
    }


    /**
     * 新增文章
     *
     * @param vo
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<ArticleVO> add(@RequestBody ArticleVO vo) {
        ArticleVO a = iArticleService.add(vo);
        return CommonResult.success(a);
    }


    /**
     * 分页查询
     *
     * @param qo
     * @return
     */
    @RequestMapping(value = "/pageQuery", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult<IPage<Article>> pageQuery(@RequestBody ArticleQo qo) {
        IPage<Article> page = iArticleService.pageQuery(qo);
        return CommonResult.success(page);
    }

}
