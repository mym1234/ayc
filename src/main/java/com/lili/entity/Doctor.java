package com.lili.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author mym
 * @since 2023-10-22
 */
@Data
@TableName("doctor")
public class Doctor implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 医生名称
     */
    private String doctorName;

    /**
     * 医生类型 1矫正  2种植 3全科
     */
    private Integer doctorType;

    /**
     * 排序时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date sortTime;

    /**
     * 上架状态
     */
    private Boolean isPublished;

    /**
     * 医生标签
     */
    private String doctorTags;

    /**
     * 医生头像
     */
    private String doctorAvatarUrl;

    /**
     * 坐诊医院
     */
    private String hospitalName;

    /**
     * 学历
     */
    private String education;

    /**
     * 执业经历
     */
    private String practiceExperience;

    /**
     * 职称
     */
    private String professionalTitle;

    /**
     * 学术研究
     */
    private String researchInterests;

    /**
     * 社会任职
     */
    private String socialPositions;

    /**
     * 矫治技术
     */
    private String orthodonticTechnique;

    /**
     * 添加人
     */
    private String addedBy;

    /**
     * 城市id
     */
    private Long cityId;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdAt;

    /**
     * 修改时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updatedAt;

    /**
     * 是否上首页
     */
    private Boolean isFeatured;

    /**
     * 性别 1男  2女
     */
    private Integer gender;


}
