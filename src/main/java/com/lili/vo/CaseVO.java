package com.lili.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CaseVO {

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 案例名称
     */
    private String caseName;

    /**
     * 医生姓名
     */
    private String doctorName;

    /**
     * 排序时间
     */
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String sortTime;

    /**
     * 上架状态
     */
    private Boolean isPublished;

    /**
     * 案例描述
     */
    private String caseDescription;

    /**
     * 案例照片
     */
    private String caseImageUrl;

    /**
     * 添加人
     */
    private String addedBy;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdAt;

    /**
     * 修改时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updatedAt;

    /**
     * 医生id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long doctorId;

    /**
     * 性别 1男  2女
     */
    private Integer gender;

    /**
     * 医生头像
     */
    private String doctorAvatarUrl;

    /**
     * 城市id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long cityId;

    /**
     * 城市名称
     */
    private String cityName;

}
