package com.lili.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author: mym
 * @description: QueryPriceRecordVO
 * @date: 2023/10/27 14:46
 */
@Data
public class QueryPriceRecordVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 手机号码
     */
    private String phoneNumber;

    /**
     * 查询城市
     */
    private String cityName;

    /**
     * 牙齿情况
     */
    private String dentalCondition;

    /**
     * 查询时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date queryTime;

    /**
     * 最后访问IP
     */
    private String lastAccessIp;

    /**
     * 状态 0未标记  1标记
     */
    private Integer status;
}
