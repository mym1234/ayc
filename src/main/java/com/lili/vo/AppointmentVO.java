package com.lili.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

/**
 * @author: mym
 * @description: AppointmentVO
 * @date: 2023/10/27 14:46
 */
@Data
public class AppointmentVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 预约医生
     */
    private String doctorName;

    /**
     * 预约人
     */
    private String patientName;

    /**
     * 手机号码
     */
    private String phoneNumber;

    /**
     * 预约时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date appointmentTime;

    /**
     * 授权号码
     */
    private String authorizationNumber;

    /**
     * 下单时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date orderTime;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdAt;

    /**
     * 修改时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updatedAt;

    /**
     * 预约医生Id
     */
    private Long doctorId;

    /**
     * 预约医院Id
     */
    private Long hospitalId;

}
