package com.lili.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.lili.entity.Hospital;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class DoctorVO {

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 医生名称
     */
    private String doctorName;

    /**
     * 医生类型 1矫正  2种植 3全科
     */
    private Integer doctorType;

    /**
     * 排序时间
     */
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String sortTime;

    /**
     * 上架状态
     */
    private Boolean isPublished;

    /**
     * 医生标签
     */
    private String doctorTags;

    /**
     * 医生头像
     */
    private String doctorAvatarUrl;

    /**
     * 坐诊医院
     */
    private String hospitalName;

    /**
     * 学历
     */
    private String education;

    /**
     * 执业经历
     */
    private String practiceExperience;

    /**
     * 职称
     */
    private String professionalTitle;

    /**
     * 学术研究
     */
    private String researchInterests;

    /**
     * 社会任职
     */
    private String socialPositions;

    /**
     * 矫治技术
     */
    private String orthodonticTechnique;

    /**
     * 添加人
     */
    private String addedBy;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdAt;

    /**
     * 修改时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updatedAt;

    /**
     * 医院列表
     */
    private List<Hospital> hospitalList;

    /**
     * 城市id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long cityId;

    /**
     * 是否上首页
     */
    private Boolean isFeatured;

    /**
     * 性别 1男  2女
     */
    private Integer gender;

    /**
     * 城市名称
     */
    private String cityName;

}
