package com.lili.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class ArticleVO {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 文章名称
     */
    private String articleName;

    /**
     * 文章图片
     */
    private String articleImg;

    /**
     * 文章内容
     */
    private String articleContent;

    /**
     * 是否上首页
     */
    private Boolean isFeatured;

    /**
     * 排序时间
     */
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String sortTime;

    /**
     * 上架状态
     */
    private Boolean isPublished;

    /**
     * 添加人
     */
    private String addedBy;

    /**
     * 初始阅读数
     */
    private Integer initRead;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdAt;

    /**
     * 修改时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updatedAt;

}
