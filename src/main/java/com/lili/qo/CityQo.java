package com.lili.qo;

import lombok.Data;

@Data
public class CityQo {

    /**
     * 每页条数
     */
    private long size;

    /**
     * 当前页数
     */
    private long current;

    /**
     * 关键字
     */
    private String key;

    /**
     * 是否上首页
     */
    private Boolean isFeatured;

    /**
     * 上架状态
     */
    private Boolean isPublished;



}
