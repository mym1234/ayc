package com.lili.qo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class CaseQo {

    /**
     * 每页条数
     */
    private long size;

    /**
     * 当前页数
     */
    private long current;

    /**
     * 关键字
     */
    private String key;

    /**
     * 上架状态
     */
    private Boolean isPublished;

    /**
     * 医生id
     */
    private Long doctorId;

    /**
     * 城市id
     */
    private Long cityId;



}
