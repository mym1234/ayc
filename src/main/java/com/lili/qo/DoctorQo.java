package com.lili.qo;

import lombok.Data;

@Data
public class DoctorQo {

    /**
     * 每页条数
     */
    private long size;

    /**
     * 当前页数
     */
    private long current;

    /**
     * 关键字
     */
    private String key;

    /**
     * 医生类型 1矫正  2种植 3全科
     */
    private Integer doctorType;

    /**
     * 上架状态
     */
    private Boolean isPublished;

    /**
     * 城市id
     */
    private Long cityId;

    /**
     * 是否上首页
     */
    private Boolean isFeatured;

    /**
     * 小程序关键字
     */
    private String extKey;



}
