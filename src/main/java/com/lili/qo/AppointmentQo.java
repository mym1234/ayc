package com.lili.qo;

import lombok.Data;

@Data
public class AppointmentQo {

    /**
     * 每页条数
     */
    private long size;

    /**
     * 当前页数
     */
    private long current;

    /**
     * 关键字
     */
    private String key;

    /**
     * 预约医生
     */
    private String doctorName;

    /**
     * 预约医生Id
     */
    private Long doctorId;


}
