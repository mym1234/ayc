package com.lili.qo;

import lombok.Getter;
import lombok.Setter;


/**
 * 用户登录参数
 *
 * @author maym <mym_1234@163.com>
 * @version 1.0.0
 * @date 2019/12/3 15:46
 * @since 1.0.0
 */
@Getter
@Setter
public class UserLoginQo {

    private String userName;

    private String password;

}
