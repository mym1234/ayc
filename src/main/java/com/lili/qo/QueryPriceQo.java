package com.lili.qo;

import lombok.Data;

import java.util.Date;

@Data
public class QueryPriceQo {

    /**
     * 每页条数
     */
    private long size;

    /**
     * 当前页数
     */
    private long current;

    /**
     * 关键字
     */
    private String key;

    /**
     * 开始时间 结束时间
     */
    private Date start;

    /**
     * 开始时间 结束时间
     */
    private Date end;


}
