package com.lili.common;

/**
 * 封装API的错误码
 * Created by mym on 2021/1/3.
 */
public interface IErrorCode {
    long getCode();

    String getMessage();
}
